package org.usfirst.frc.team5926.robot;

import edu.wpi.first.wpilibj.CANTalon;
import edu.wpi.first.wpilibj.CameraServer;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.PowerDistributionPanel;
import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the IterativeRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the manifest file in the resource
 * directory.
 */
public class Robot extends IterativeRobot { // The code behind MooBot's antics
	boolean down;
	boolean autoFin;
	CameraServer server;
	CANTalon frontLeft, frontRight, rearLeft, rearRight; // Drive motors
	DigitalInput LS1;
	DigitalInput LS2;
	DigitalInput LS3;
	DigitalInput LS4;
	DigitalInput LS5;
	double rampartVar0;
	double rampartVar1;
	double rampartVar2;
	double rampartVar3;
	double rampartVar4;
	double liftPower;
	double autoSel;
	double autoLoopNum;
	double crossLoop;
	Joystick driveStick; // Logitech Joystick 0
	Joystick specialControlsStick; // Logitech Joystick 1
	PowerDistributionPanel PDP;
	RobotDrive driveTrain;
	Solenoid Brake;
	VictorSP liftArm;
	
    /**
     * This function is run when the robot is first started up and should be
     * used for any initialization code.
     */
	
    public void robotInit() { // Training MooBot to do your bidding...
    	// Motor setup
    	frontLeft = new CANTalon(1);
    	rearLeft = new CANTalon(2);
    	frontRight = new CANTalon(3);
    	rearRight= new CANTalon(4);
    	liftArm = new VictorSP(0);
    	driveTrain = new RobotDrive(frontLeft, rearLeft, frontRight, rearRight);
    	// Limit Switch setup
    	LS1 = new DigitalInput(1);
    	LS2 = new DigitalInput(2);
    	LS3 = new DigitalInput(3);
    	LS4 = new DigitalInput(4);
    	LS5 = new DigitalInput(5);
    	// Controller setup
    	driveStick = new Joystick(0);
    	specialControlsStick = new Joystick(1);
    	// Set up USB Camera
    	server = CameraServer.getInstance();
        server.setQuality(50);
        server.startAutomaticCapture("cam0"); // The name is in the roboRIO web interface
        // PDP setup
        PDP = new PowerDistributionPanel();
        Brake = new Solenoid(1);
        //SmartDashboard.putNumber("AUTON_SELECT", 0);
        //Steve was here. He is really handsome. Everyone wishes they could be more like Steve.
    }
    
    /**
     * This function is run once each time the robot enters disabled mode
     */
    
    public void disabledInit() {
    	Brake.set(false);
    }
    
    /**
     * This function is called periodically during disabled mode
     */
    
    public void disabledPeriodic() {
    	// SmartDashboard Commands
    	/*SmartDashboard.putNumber("rampartVarReachLoop", 0);
    	SmartDashboard.putNumber("rampartVarReachSpeed", 0);
    	SmartDashboard.putNumber("rampartVarCrossLoop", 0);
    	SmartDashboard.putNumber("rampartVarCrossPwrLeft", 0);
    	SmartDashboard.putNumber("rampartVarCrossPwrRight", 0);*/
    }
    
    /**
     * This function is run once each time the robot enters autonomous mode
     */
    
    public void autonomousInit() {
    	down = false;
    	autoFin = false;
    	autoLoopNum = 0;
    	crossLoop = 0;

    	/*// rampart
    	rampartVar0 = SmartDashboard.getNumber("rampartVarReachLoop", 0);
    	rampartVar1 = SmartDashboard.getNumber("rampartVarReachSpeed", 0);*/
    	rampartVar2 = SmartDashboard.getNumber("rampartVarCrossLoop", 0);
    	rampartVar3 = SmartDashboard.getNumber("rampartVarCrossPwrLeft", 0);
    	rampartVar4 = SmartDashboard.getNumber("rampartVarCrossPwrRight", 0);

    	autoSel = SmartDashboard.getNumber("AUTON_SELECT", 0);
    }

    /**
     * This function is called periodically during autonomous
     */
    public void auton1() {
    	if (autoLoopNum <= 61) {
    		driveTrain.tankDrive(0.32, 0.32, false);
    		if (LS4.get() == true) {
    			if (autoLoopNum <= 30) {
    	    		liftArm.set(-0.25);
    	    	}
    			else {
    			liftArm.set(-0.65);
    			}
    		}
    		else {
    			liftArm.set(0.0);
    		}
    	}
    	else if (autoLoopNum >= 61 && down == false){
    		if (LS3.get() == true) {
    			liftArm.set(0.25);
    		}
    		else {
    			down = true;
    			liftArm.set(0.0);
    		}
    		driveTrain.tankDrive(0.0, 0.0, false);
    	}
    	else if (down == true && autoFin == false) {
    		if (LS4.get() == true) {
    			liftArm.set(-0.25);
    		}
    		else {
    			liftArm.set(0.0);
    		}
    		if (crossLoop <= 85) {
    			driveTrain.tankDrive(0.54, 0.54, false);
    		}
    		if (crossLoop >= 85 && crossLoop <= 100) {
    			liftArm.set(0.0);
    			driveTrain.tankDrive(0.0, 0.35, false);
    		}
    		if (crossLoop >= 100) {
    			autoFin = true;
    		}
    		crossLoop++;
    	}
    	if (autoFin == true) {
    		driveTrain.tankDrive(0.0, 0.0, false);
    	}
    	autoLoopNum++;
    }
    public void auton2() {
    	if (autoLoopNum <= 60) {
    		driveTrain.tankDrive(0.36, 0.4, false);
    	}
    	else {
    		driveTrain.tankDrive(0.0, 0.0, false);
    	}
    	autoLoopNum++;
    }
    public void auton3() {
    	Brake.set(true);
    	if (autoLoopNum <= 50) {
			driveTrain.tankDrive(0.7, 0.7, false);
		}
    	else if (autoLoopNum >= 50 && autoLoopNum <= 200) {
    		driveTrain.tankDrive(0.7, 0.8, false);
    	}
    	else if (autoLoopNum >= 500 && autoLoopNum <= 575 && LS2.get() == true || LS5.get() == true) {
    		liftArm.set(-0.15);
    		driveTrain.tankDrive(0.0, 0.0, false);
    	}
		else {
			liftArm.set(0.0);
			driveTrain.tankDrive(0.0, 0.0, false);
		}
		autoLoopNum++;
    }
    public void auton4() {
    	Brake.set(true);
		if (autoLoopNum <= 220) {
			driveTrain.tankDrive(0.2, 0.2, false);
			liftArm.set(-0.15);
		}
		else {
			liftArm.set(0.0);
			driveTrain.tankDrive(0.0, 0.0, false);
		}
		autoLoopNum++;
    }
    public void auton5() {
    	Brake.set(true);
    	if (autoLoopNum <= 190) {
			driveTrain.tankDrive(0.5, 0.5, false);
			liftArm.set(-0.15);
		}
		else {
			liftArm.set(0.0);
			driveTrain.tankDrive(0.0, 0.0, false);
		}
		autoLoopNum++;
    }
    public void auton6() {
    	Brake.set(true);
    	if (autoLoopNum <= 220 && LS2.get() == true && LS5.get() == true) {
			liftArm.set(-0.15);
			driveTrain.tankDrive(0.0, 0.0, false);
		}
		else {
			liftArm.set(0.0);
			driveTrain.tankDrive(0.0, 0.0, false);
		}
		autoLoopNum++;
    }
    public void auton7() {
    	Brake.set(true);
    	if (autoLoopNum <= 100) {
			driveTrain.tankDrive(0.7, 0.7, false);
		}
    	else if (autoLoopNum >= 75 && autoLoopNum <= 175 && LS2.get() == true && LS5.get() == true) {
    		liftArm.set(-0.15);
    		driveTrain.tankDrive(0.0, 0.0, false);
    	}
		else {
			liftArm.set(0.0);
			driveTrain.tankDrive(0.0, 0.0, false);
		}
		autoLoopNum++;
    }
    public void auton8() {
    	Brake.set(true);
    	if (autoLoopNum <= 100) {
			driveTrain.tankDrive(0.9, 0.9, false);
		}
    	else if (autoLoopNum >= 100 && autoLoopNum <= 200 && LS2.get() == true && LS5.get() == true) {
    		liftArm.set(-0.15);
    		driveTrain.tankDrive(0.0, 0.0, false);
    	}
		else {
			liftArm.set(0.0);
			driveTrain.tankDrive(0.0, 0.0, false);
		}
		autoLoopNum++;
    }
    public void autonomousPeriodic() {
    	if (autoSel == 0) {
    	}
    	else if (autoSel == 1) {
    		//auton1();
    	}
    	else if (autoSel == 2) {
    		//auton2();
    	}
    	else if (autoSel == 3) {
    		auton3();
    	}
    	else if (autoSel == 4) {
    		auton4();
    	}
    	else if (autoSel == 5) {
    		auton5();
    	}
    	else if (autoSel == 6) {
    		auton6();
    	}
    	else if (autoSel == 7) {
    		auton7();
    	}
    	else if (autoSel == 8) {
    		auton8();
    	}
    	else {
    	}
    	SmartDashboard.putNumber("AUTO_LOOP_COUNT", autoLoopNum);
    }
    
    /**
     * This function is called once each time the robot enters tele-operated mode
     */
    
    public void teleopInit(){
    }

    /**
     * This function is called periodically during operator control
     */
    
    public void teleopPeriodic() { // MooBot becomes your slave...
    	// Drive Commands..
    	if (driveStick.getTrigger() == true) { // Speed boost mode >---> >--->
    		driveTrain.arcadeDrive(-driveStick.getY()*0.85,-driveStick.getX()*0.85);
    	}
    	else if (driveStick.getRawButton(2) == true) { // Moat cross mode >---> >--->
    		driveTrain.arcadeDrive(-driveStick.getY()*0.95,-driveStick.getX()*0.35);
    	}
    	else { // Snail mode -> -> -> -> ->
    		driveTrain.arcadeDrive(-driveStick.getY()*0.5,-driveStick.getX()*0.70);
    	}
    	// End Drive Commands
    	
    	// Lift Controls
    	// Lift Break Solenoid 
    	if (specialControlsStick.getY() < -0.01 || specialControlsStick.getY() > 0.01) {
    		Brake.set(true);
    	}
    	else {
    		Brake.set(false);
    	}
    	
    	if (LS1.get() == false && LS3.get() == false) {
    		SmartDashboard.putBoolean("LOW_LIMIT", true);
    		if (-specialControlsStick.getY() > 0.0) {
    			liftPower = 0.0;
    			liftArm.set(liftPower);
    		}
    		else {
    			liftPower = -specialControlsStick.getY();
    		}
    		
    	}
    	else if (LS5.get() == false && specialControlsStick.getTrigger() == false) {
    		SmartDashboard.putBoolean("SOFT_HIGH_LIMIT", true);
    		if (-specialControlsStick.getY() < 0.0) {
    			liftPower = 0.0;
    			liftArm.set(liftPower);
    		}
    		else {
    			liftPower = -specialControlsStick.getY();
    		}
    	}
    	else if (LS2.get() == false && LS4.get() == false) {
    		SmartDashboard.putBoolean("HIGH_LIMIT", true);
    		if (-specialControlsStick.getY() < 0.0) {
    			liftPower = 0.0;
    			liftArm.set(liftPower);
    		}
    		else {
    			liftPower = -specialControlsStick.getY();
    		}
    	}
    	else {
    		SmartDashboard.putBoolean("LOW_LIMIT", false);
    		SmartDashboard.putBoolean("SOFT_HIGH_LIMIT", false);
    		SmartDashboard.putBoolean("HIGH_LIMIT", false);
    		liftPower = -specialControlsStick.getY()*0.5;
    	}
    	//
    	liftArm.set(liftPower);
    	// End Lift Controls
    	
    	// SmartDashboard Commands
    	SmartDashboard.putNumber("PDP_TEMP", PDP.getTemperature());
    }
    
    /**
     * This function is called periodically during test mode
     */
    
    public void testPeriodic() {
    	LiveWindow.run();
    }
    
}
